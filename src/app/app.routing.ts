import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    {
        path: '', 
        pathMatch: 'full',
        loadChildren: () => import('./pages/main/main.module').then(m => m.MainPageModule)
    },
    {
        path: 'login', 
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'panel', 
        loadChildren: () => import('./pages/admin/panel/AdminPanel.module').then(m => m.AdminPanelModule)
    },
    {
        path: 'add-site', 
        loadChildren: () => import('./pages/admin/add-site/AddSite.module').then(m => m.AddSiteModule)
    },
    {
        path: 'view/:id', 
        loadChildren: () => import('./pages/site/SiteView.module').then(m => m.SiteViewPageModule)
    },
    {
        path: 'edit/:id', 
        loadChildren: () => import('./pages/admin/edit-site/edit-site.module').then(m => m.EditSiteModule)
    }
  ];
@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}