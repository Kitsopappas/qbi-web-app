import { RestApiConnector } from './RestApiConnector';
import { Constants } from './constants';
import { Observable } from 'rxjs';
import { LoginStatus } from '../models/User';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class LoginService
{
    private api: RestApiConnector;
    constructor(http: HttpClient){
        this.api = new RestApiConnector(http);
    }

    login(body: {email: string, password: string}): Observable<LoginStatus>
    {
        return new Observable(observer => {
            this.api.post(Constants.ApiCall.QBI_USERS_LOGIN, body).subscribe(
                data => 
                {
                    let loginStatus = new LoginStatus(data['success'], data['message'], data['token'], data['time']);
                    observer.next(loginStatus);
                    observer.complete();
                },
                err => 
                {
                    let loginStatus = new LoginStatus(false, err.message, null, null);
                    observer.next(loginStatus);
                    observer.complete();
                },
                () => {console.log('Login operation complete')}
            );
        });
    }

    loginWithSocial(body: {providerKey: string, providerType: number}): Observable<LoginStatus>
    {
        return new Observable(observer => {
            this.api.post(Constants.ApiCall.QBI_USERS_LOGIN_SOCIAL, body).subscribe(
                data => 
                {
                    let loginStatus = new LoginStatus(data['success'], data['message'], data['token'], data['time']);
                    observer.next(loginStatus);
                    observer.complete();
                },
                err => 
                {
                    let loginStatus = new LoginStatus(false, err.message, null, null);
                    observer.next(loginStatus);
                    observer.complete();
                },
                () => {console.log('Login operation complete')}
            );
        });
    }
}