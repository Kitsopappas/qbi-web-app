import {  FileItem, FileUploader } from 'ng2-file-upload';
import { Subject } from 'rxjs';
import { Constants } from './constants';
import { LocalStorage } from './LocalStorage';

export class FileUploaderService
{
    private imageUploader: FileUploader;
    private localStorage: LocalStorage;

    public onFileAdded = new Subject<FileItem>();
    public onFileUploaded = new Subject<any>();
    constructor()
    {
        this.localStorage = new LocalStorage();
        const token: string = this.localStorage.get(Constants.LocalStorageKey.QBI_LOGIN_TOKEN);
        this.imageUploader = new FileUploader({
            url: Constants.ApiUrl + Constants.ApiCall.QBI_UPLOAD_IMAGE, 
            itemAlias: 'photo', 
            autoUpload: true, 
            authToken: token, 
            authTokenHeader: "authorization",
            allowedMimeType: ['image/jpeg', 'image/png', 'image/jpg']
        });
        this.subscribeToFileAdd();
        this.subscribeToFileUploaded();

        this.imageUploader.onAfterAddingFile = (item => {
            item.withCredentials = false;
         });
    }

    private subscribeToFileAdd()
    {
        this.imageUploader.onAfterAddingFile = (file) => {
            this.onFileAdded.next(file);
        };
    }

    private subscribeToFileUploaded()
    {
        this.imageUploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            this.onFileUploaded.next({item: item, response: response, status: status});
        };
    }

    getUploader(): FileUploader
    {
        return this.imageUploader;
    }
}