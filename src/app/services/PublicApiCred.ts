import { RestApiConnector } from './RestApiConnector';
import { Constants } from './constants';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiCredentials } from '../models/ApiCredentials';

@Injectable({
    providedIn: 'root',
})

export class PublicApiService
{
    private api: RestApiConnector;
    constructor(http: HttpClient){
        this.api = new RestApiConnector(http);
    }

    getApiCredentialsForUser(): Observable<ApiCredentials>
    {
        return new Observable<ApiCredentials>(observer => {
            this.api.get(Constants.ApiCall.QBI_APP_CRED_PUBLIC_API).subscribe(
                data => 
                {
                    let publicApiCredentials = new ApiCredentials(data['app'], data['publicKey'], data['privateKey'], data['createdAt'], data['updatedAt']);
                    observer.next(publicApiCredentials);
                    observer.complete();
                },
                err => 
                {
                    observer.next(null);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }

    createApiCredentialsForUser(body: {app: string}): Observable<ApiCredentials>
    {
        return new Observable<ApiCredentials>(observer => {
            this.api.post(Constants.ApiCall.QBI_APP_CRED_PUBLIC_API, body).subscribe(
                data => 
                {
                    let publicApiCredentials = new ApiCredentials(data['app'], data['publicKey'], data['privateKey'], data['createdAt'], data['updatedAt']);
                    observer.next(publicApiCredentials);
                    observer.complete();
                },
                err => 
                {
                    observer.next(null);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }
}