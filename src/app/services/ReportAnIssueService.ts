import { RestApiConnector } from './RestApiConnector';
import { Constants } from './constants';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IssueReport } from '../models/IssueReport';

@Injectable({
    providedIn: 'root',
})

export class ReportAnIssueService
{
    private api: RestApiConnector;
    constructor(http: HttpClient){
        this.api = new RestApiConnector(http);
    }

    reportAnIssue(body: IssueReport): Observable<IssueReport>
    {
        return new Observable<IssueReport>(observer => {
            this.api.post(Constants.ApiCall.QBI_ISSUE_REPORT, body).subscribe(
                data => 
                {
                    let res = new IssueReport(data['type'], data['siteId'], data['reporterEmail'], data['title'], data['message']);
                    observer.next(res);
                    observer.complete();
                },
                err => 
                {
                    observer.next(null);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }
}