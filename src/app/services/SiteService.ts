import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContactInfo, Coordinates, Image, Section, Site } from '../models/Site';
import { Constants } from './constants';
import { RestApiConnector } from './RestApiConnector';

@Injectable({
    providedIn: 'root',
})

export class SiteService
{
    private api: RestApiConnector;
    constructor(http: HttpClient){
        this.api = new RestApiConnector(http);
    }

    initSite(body: {credential: string, latitude: number, longitude: number}): Observable<{credential: string, latitude: number, longitude: number}>
    {
        return new Observable(observer => {
            this.api.post(Constants.ApiCall.QBI_SITE_INIT, body).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }

    createSite(site: Site): Observable<any>
    {
        let body = {
            siteAttr:
            {
                id: site.getId(),
                credential: site.getCredential(),
                latitude: site.getcoordinates().getLatitude(),
                longitude: site.getcoordinates().getLongitude()
            },
            contactAttr: this.getContactInfoForSite(site),
            imagesAttr: this.getImagesForSite(site),
            sectionsAttr: this.getSectionsForSite(site)
        };

        return new Observable(observer => {
            this.api.post(Constants.ApiCall.QBI_SITE_UPDATE, body).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Create operation complete')
            );
        });
    }

    updateSite(site: Site): Observable<any>
    {
        let body = {
            siteAttr:
            {
                id: site.getId(),
                credential: site.getCredential(),
                latitude: site.getcoordinates().getLatitude(),
                longitude: site.getcoordinates().getLongitude()
            },
            contactAttr: this.getContactInfoForSite(site),
            imagesAttr: this.getImagesForSite(site),
            sectionsAttr: this.getSectionsForSite(site)
        };

        return new Observable(observer => {
            this.api.put(Constants.ApiCall.QBI_SITE_UPDATE, body).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Create operation complete')
            );
        });
    }

    getUserSites()
    {
        return new Observable(observer => {
            this.api.get(Constants.ApiCall.QBI_SITE_GET).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    getSiteById(id: number)
    {
        return new Observable(observer => {
            this.api.get(Constants.ApiCall.QBI_SITE_GET_BY_ID + id).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    getNearestSites(coord: Coordinates, accuracy: number)
    {
        return new Observable(observer => {
            this.api.get(`${Constants.ApiCall.QBI_SITE_GET_NEAR}${coord.getLatitude()}/${coord.getLongitude()}/${accuracy}`).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    deleteImage(id: number)
    {
        return new Observable(observer => {
            this.api.delete(`${Constants.ApiCall.QBI_SITE_DELETE_IMAGE}${id}`).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.error(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    deleteContact(id: number)
    {
        return new Observable(observer => {
            this.api.delete(`${Constants.ApiCall.QBI_SITE_DELETE_CONTACT}${id}`).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    deleteSection(id: number)
    {
        return new Observable(observer => {
            this.api.delete(`${Constants.ApiCall.QBI_SITE_DELETE_SECTION}${id}`).subscribe(
                (data: any) => 
                {
                    observer.next(data);
                    observer.complete();
                },
                err => 
                {
                    observer.next(err);
                    observer.complete();
                },
                () => console.log('Get operation complete')
            );
        });
    }

    private getImagesForSite(site: Site)
    {
        let array = [];
        var images: Array<Image> = site.getImages();
        for (var i = 0; i < images.length; i++)
        {
            let img: Image = images[i];
            let obj = {
                alt: img.getAlt(),
                file: img.getFile(),
                is_main: img.getIsMain()
            };
            if (img.getId() >= 0)
            {
                obj["id"] = img.getId();
            }
            array.push(obj);
        }
        return array;
    }

    private getContactInfoForSite(site: Site)
    {
        let contactInfoArray = [];
        var contactInfo: Array<ContactInfo> = site.getContactInfo();
        for (var i = 0; i < contactInfo.length; i++)
        {
            let cnt: ContactInfo = contactInfo[i];
            let obj = {
                type: cnt.getType(),
                contact: cnt.getContact(),
                language: cnt.getLanguage()
            };
            if (cnt.getId() >= 0)
            {
                obj["id"] = cnt.getId();
            }
            contactInfoArray.push(obj);
        }
        return contactInfoArray;
    }

    private getSectionsForSite(site: Site)
    {
        let array = [];
        var sectionsInfo: Array<Section> = site.getSections();
        for (var i = 0; i < sectionsInfo.length; i++)
        {
            let section: Section = sectionsInfo[i];
            let obj = {
                title: section.getTitle(),
                content: section.getContent(),
                language: section.getLanguage(),
                level: section.getLevel()
            };
            if (section.getId() >= 0)
            {
                obj["id"] = section.getId();
            }
            array.push(obj);
        }
        return array;
    }
}
