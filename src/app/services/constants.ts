const isProduction = true;

export class Constants
{
	public static ApiUrl: string = isProduction ? "https://api.theqbi.com/api/v1/" : "http://localhost:3000/api/v1/";

	public static ApiCall =
	{
		QBI_USERS_LOGIN: "users/login/", //post
		QBI_USERS_LOGIN_SOCIAL: "users/oauth/", //post
		QBI_USERS_SIGN_UP: "users/", //post
		QBI_USERS_CURRENT: "users/", //get
		QBI_USERS_EMAIL: "users/$/", // get
		QBI_USERS_UPDATE: "users", // put
		QBI_SITE_INIT: "sites/init", // post
		QBI_SITE_CREATE: "sites/", // post
		QBI_SITE_UPDATE: "sites/", // put
		QBI_SITE_GET_BY_ID: "sites/", // get
		QBI_SITE_GET_NEAR: "sites/location/", // get
		QBI_SITE_GET: "sites/", //get
		QBI_UPLOAD_IMAGE: "upload/",
		QBI_SITE_IMAGES: "images/",
		QBI_SITE_DELETE_IMAGE: "sites/image/",
		QBI_SITE_DELETE_SECTION: "sites/section/",
		QBI_SITE_DELETE_CONTACT: "sites/contact/",
		QBI_APP_CRED_PUBLIC_API: "app-cred/", // get, post
		QBI_ISSUE_REPORT: "issue-report/",
		QBI_AUTHENTICATION_PROVIDER: "authentication-provider/",
		QBI_AUTHENTICATION_PROVIDER_AVAILABLE: "authentication-provider/user-available/"
	};

	public static LocalStorageKey = 
	{
		QBI_LOGIN_TOKEN: "kQBI_LOGIN_TOKEN"
	};

	public static SocialMedia = 
	{
		facebook: {
			clientID: "275948673771705"
		},
		google: {
			clientID: "465712067893-t6meu91nd4a4geg41udf7fud73cjh2fb.apps.googleusercontent.com"
		}
	};
}