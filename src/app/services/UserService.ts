import { RestApiConnector } from './RestApiConnector';
import { Constants } from './constants';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthProvider } from '../models/AuthProvider';

@Injectable({
    providedIn: 'root',
})

export class UserService {
    private api: RestApiConnector;
    constructor(http: HttpClient) {
        this.api = new RestApiConnector(http);
    }

    currentUser(): Observable<User> {
        return new Observable(observer => {
            this.api.get(Constants.ApiCall.QBI_USERS_CURRENT).subscribe(
                data => {
                    let user = new User(data['email'], data['createdAt'], data['updatedAt'], data['deletedAt'], data['UserType']['type']);
                    observer.next(user);
                    observer.complete();
                },
                err => {
                    observer.error(err);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }

    createUser(body: { email: string, password: string, type: number }): Observable<User> {
        return new Observable(observer => {
            this.api.post(Constants.ApiCall.QBI_USERS_SIGN_UP, body).subscribe(
                data => {
                    let user = new User(data['email'], data['createdAt'], data['updatedAt'], data['deletedAt'], 'User');
                    observer.next(user);
                    observer.complete();
                },
                err => {
                    observer.error(err);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }

    createAuthenticationProvider(body: { providerKey: string, providerType: number }): Observable<AuthProvider> {
        return new Observable<AuthProvider>(observer => {
            this.api.post(Constants.ApiCall.QBI_AUTHENTICATION_PROVIDER, body).subscribe(
                data => {
                    let provider = new AuthProvider(data['providerKey'], data['providerType']);
                    observer.next(provider);
                    observer.complete();
                },
                err => {
                    observer.error(err);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }

    getUserAvailableAuthenticationProviders(): Observable<Array<AuthProvider>> {
        return new Observable<Array<AuthProvider>>(observer => {
            this.api.get(Constants.ApiCall.QBI_AUTHENTICATION_PROVIDER_AVAILABLE).subscribe(
                (data: Array<any>) => {
                    let providers = new Array<AuthProvider>();
                    if (data) {
                        for (let i = 0; i < data.length; i++) {
                            let provider = new AuthProvider(data[i]['providerKey'], data[i]['providerType']);
                            providers.push(provider);
                        }
                    }
                    observer.next(providers);
                    observer.complete();
                },
                err => {
                    observer.error(err);
                    observer.complete();
                },
                () => console.log('Login operation complete')
            );
        });
    }
}