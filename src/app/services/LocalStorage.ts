import { Constants } from './constants';

export class LocalStorage
{
    set(key: string, value: string)
    {
        if (!Constants.LocalStorageKey[key])
        {
            console.error(`Could not set key ${key}. Does not exist in available QBI keys`);
        }
        localStorage.setItem(key, value);
    }

    get(key: string)
    {
        return localStorage.getItem(key);
    }
}