import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from './constants';
import { LocalStorage } from './LocalStorage';

@Injectable({
    providedIn: 'root',
})

export class RestApiConnector
{
    private localStorage: LocalStorage;
    private httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    constructor(private http: HttpClient)
    {
        this.localStorage = new LocalStorage();
    }

    post(apiCall: string, body: any)
    {
        return this.http.post(Constants.ApiUrl + apiCall, body, {headers: this.setToken()});
    }

    get(apiCall: string)
    {
        return this.http.get(Constants.ApiUrl + apiCall, {headers: this.setToken()});
    }

    put(apiCall: string, body: any)
    {
        return this.http.put(Constants.ApiUrl + apiCall, body, {headers: this.setToken()});
    }

    delete(apiCall: string)
    {
        return this.http.delete(Constants.ApiUrl + apiCall, {headers: this.setToken()});
    }

    private setToken(): HttpHeaders
    {
        const token: string = this.localStorage.get(Constants.LocalStorageKey.QBI_LOGIN_TOKEN);
        if (token)
        {
            return this.httpOptions.headers.append("authorization", token);
        }
        else
        {
            return this.httpOptions.headers;
        }
    }
}