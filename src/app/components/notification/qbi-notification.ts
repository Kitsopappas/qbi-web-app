import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'qbi-notification',
    templateUrl: './qbi-notification.html',
    styleUrls: ['./qbi-notification.scss']
})
export class NotificationComponent implements OnInit {
    @Input('message') message: string;
    @Input('type') type: string;

    className: string;
    iconName: string;

    constructor() {}

    ngOnInit(): void
    {
        this.className = this.getClassName(this.type);
        this.iconName = this.getIcon(this.type);
    }

    private getIcon(type: string): string {
        let icon = 'check';
        switch (type) {
            case 'info':
                icon = 'info_outline';
                break;
            case 'success':
                icon = 'check';
                break;
            case 'warning':
                icon = 'warning';
                break;
            case 'error':
                icon = 'error_outline';
                break;
        }

        return icon;
    }

    private getClassName(type: string): string
    {
        // [ngClass]="{'active': i == 0}"
        let cls = 'check';
        switch (type) {
            case 'info':
                cls = 'alert-info';
                break;
            case 'success':
                cls = 'alert-success';
                break;
            case 'warning':
                cls = 'alert-warning';
                break;
            case 'error':
                cls = 'alert-danger';
                break;
        }

        return cls;
    }
}