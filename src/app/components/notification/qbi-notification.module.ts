import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationComponent } from './qbi-notification';

@NgModule({
    imports: [CommonModule],
    exports: [NotificationComponent],
    declarations: [NotificationComponent],
    providers: [],
})
export class NotificationModule {
}