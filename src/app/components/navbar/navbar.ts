import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';

@Component({
  selector: 'app-qbi-navbar',
  templateUrl: './navbar.html'
})
export class NavBarComponent implements OnInit{
  private userViewModel: UserViewModel;

  userIsLoggedIn: boolean = false;
  constructor(private http: HttpClient, private router: Router) { 
    
  }

  ngOnInit(): void {
    this.bindWithViewModel();    
  }

  bindWithViewModel(): void
  {
    this.userViewModel = new UserViewModel(this.http);
    this.userViewModel.currentUser();
    this.bindSubscribers();
  }

  bindSubscribers(): void
  {

    this.userViewModel.currentUserSubscription.subscribe(data=>{
      if(data)
      {
        this.userIsLoggedIn = true;
      }
    });
  }

  openAdminPanel()
  {
    this.router.navigate(['/panel']);
  }

  openLoginPage()
  {
    this.router.navigate(['/login']);
  }

}
