import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavBarComponent } from './navbar';

@NgModule({
    imports: [CommonModule],
    exports: [NavBarComponent],
    declarations: [NavBarComponent],
    providers: [],
})
export class NavBarModule {
}