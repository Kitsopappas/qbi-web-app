import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
	selector: 'app-api',
	templateUrl: './app-api.html',
	styleUrls: ['./app-api.scss']
})
export class AppApiComponent implements OnInit {
	@Input('app') app: string;
	@Input('token') token: string;
	@Input('hasPublicApi') hasPublicApi: boolean;

	@Output() didTouchGetPubicApiCredentials = new EventEmitter();
	constructor() {
	}

	ngOnInit(): void { }

	sendClickEvent(): void {
		this.didTouchGetPubicApiCredentials.emit();
	}
}
