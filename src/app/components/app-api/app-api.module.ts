import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppApiComponent } from './app-api';

@NgModule({
    imports: [CommonModule],
    exports: [AppApiComponent],
    declarations: [AppApiComponent],
    providers: [],
})
export class AppApiModule {
}