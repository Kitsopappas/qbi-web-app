import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'main-image',
  templateUrl: './main-image.html'
})
export class MainImageComponent implements OnInit{
  @Input('src') src: string;
  @Input('title') title: string;
  @Input('description') description: string;
  @Input('height') height: string;
  @Input('showButton') showButton: boolean;

  constructor() { 
    if (!this.height)
      this.height = '40';
  }
   ngOnInit(): void {}
}
