import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MainImageComponent } from './main-image';

@NgModule({
    imports: [CommonModule],
    exports: [MainImageComponent],
    declarations: [MainImageComponent],
    providers: [],
})
export class MainImageModule {
}