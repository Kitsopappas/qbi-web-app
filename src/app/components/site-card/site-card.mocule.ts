import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SiteCardComponent } from './site-card';

@NgModule({
    imports: [CommonModule],
    exports: [SiteCardComponent],
    declarations: [SiteCardComponent],
    providers: [],
})
export class SiteCardModule {
}