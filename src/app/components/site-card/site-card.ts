import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'site-card',
  templateUrl: './site-card.html'
})
export class SiteCardComponent {
  @Input('image') image: string;
  @Input('title') title: string;
  @Input('content') content: string;
  @Input('id') id: string;

  constructor(private router: Router) {
  }

  visitSite()
  {
    this.router.navigate([`/view/${this.id}`]);
  }
}
