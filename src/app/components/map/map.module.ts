import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MapComponent } from './map';

@NgModule({
    imports: [CommonModule],
    exports: [MapComponent],
    declarations: [MapComponent],
    providers: [],
})
export class MapModule {
}