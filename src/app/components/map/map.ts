import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Coordinates } from 'src/app/models/Site';
import * as L from 'leaflet';

@Component({
  selector: 'qbi-map',
  templateUrl: './map.html',
  styleUrls: ['./map.scss']
})
export class MapComponent implements AfterViewInit, OnInit {
    private map;
    private region;

    @Input('latitude') latitude: number;
    @Input('longitude') longitude: number;
    @Input('zoom') zoom: number;
    @Input('allow-click') allowClick: boolean;
    
    private coordinates: Coordinates;

    @Output('clickedCoordinatesEvent') clickedCoordinatesEvent = new EventEmitter<Coordinates>();
    constructor() {
      if (this.zoom == null)
      {
        this.zoom = 12;
      }
    }

    ngOnInit() {
      this.allowClick = this.allowClick !== undefined;
    }

    ngAfterViewInit(): void {
      this.initMap();
      this.coordinates = new Coordinates(this.latitude, this.longitude);
      const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 19,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      });

      tiles.addTo(this.map);
      this.map.invalidateSize();
      if (this.allowClick)
      {
        this.map.on("click", e => {
          this.coordinates.setLatitude(e.latlng.lat);
          this.coordinates.setLongitude(e.latlng.lng);
          this.clickedCoordinatesEvent.emit(this.coordinates);
          this.updateRegion(this.coordinates);
        });
      } else {
        var myIcon = L.icon({
          iconUrl: '../assets/img/map-pointer.png',
          iconSize: [64, 64],
          iconAnchor: [22, 63] // point of the icon which will correspond to marker's location
      });
      L.marker([this.latitude, this.longitude], {icon: myIcon}).addTo(this.map);
      }
    }

    updateRegion(coord: Coordinates)
    {
      if (this.region)
      {
        this.map.removeLayer(this.region);
      }
      this.region = L.circle([coord.getLatitude(), coord.getLongitude()], {radius: 100});
      this.region.addTo(this.map);
    }

    getCoordinates(): Coordinates
    {
      return this.coordinates;
    }

    private initMap(): void {
        const center = [
          this.latitude,
          this.longitude
        ];
        this.map = L.map('maplft', {
          center: center,
          zoom: this.zoom
        });
    }
}
