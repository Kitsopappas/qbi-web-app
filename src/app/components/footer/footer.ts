import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer-com',
  templateUrl: './footer.html'
})
export class FooterComponent implements OnInit{
  year: string;
  constructor() { 
    this.year = new Date().getFullYear().toString();
  }

  ngOnInit(): void {}

}
