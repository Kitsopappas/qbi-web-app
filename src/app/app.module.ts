import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

/* Services */
import { LoginService } from './services/LoginService';
import { UserService } from './services/UserService';
import { RestApiConnector } from './services/RestApiConnector';
import { SiteService } from './services/SiteService';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploaderService } from './services/FileUploader';
import { QRCodeModule } from 'angularx-qrcode';
import { FileSaverModule } from 'ngx-filesaver';
import { AppRoutingModule } from './app.routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    QRCodeModule,
    FileSaverModule,
    FileUploadModule,
    AppRoutingModule
  ],
  providers: [RestApiConnector, LoginService, UserService, SiteService, FileUploaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
