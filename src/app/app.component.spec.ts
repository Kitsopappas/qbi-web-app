import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer';
import { MainImageComponent } from './components/main-image/main-image';
import { MapComponent } from './components/map/map';
import { NavBarComponent } from './components/navbar/navbar';
import { SiteCardComponent } from './components/site-card/site-card';


describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        AppComponent,
        NavBarComponent,
        MainImageComponent,
        FooterComponent,
        MapComponent,
        SiteCardComponent,
        Notification
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
