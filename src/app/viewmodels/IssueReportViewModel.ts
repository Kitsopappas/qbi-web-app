import { HttpClient } from '@angular/common/http';
import { ReportAnIssueService } from '../services/ReportAnIssueService';
import { Subject } from 'rxjs';
import { IssueReport } from '../models/IssueReport';

export class IssueReportViewModel {
	private service: ReportAnIssueService;

	public issueReported = new Subject<IssueReport>();

	constructor(http: HttpClient) {
        this.service = new ReportAnIssueService(http);
	}

    createNewPublicApiCredentials(body: IssueReport)
    {
        this.service.reportAnIssue(body).subscribe(
            (data: IssueReport) => {
				this.issueReported.next(data);
            });
    }
}