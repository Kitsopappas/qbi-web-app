import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Site, Coordinates, ContactInfo, Section, Image } from '../models/Site';
import { FileUploaderService } from '../services/FileUploader';
import { SiteService } from '../services/SiteService';

export class SiteViewModel
{
    private siteService: SiteService;

    site: Site;
    fileUploaderService: FileUploaderService;
    public siteInitSubscription = new Subject<Site>();
    public userSitesSubscription = new Subject<Array<Site>>();
    public nearestSitesSubscription = new Subject<Array<Site>>();

    public errorSubscription = new Subject<Error>();
    public siteCreateOrUpdateSubscription = new Subject<number>();
    public siteByIdSubscription = new Subject<Site>();
    constructor(http: HttpClient)
    {
        this.siteService = new SiteService(http);
        this.fileUploaderService = new FileUploaderService();
    }

    initSite(site: Site)
    {
        this.site = site;
        let coord: Coordinates = this.site.getcoordinates();
        this.siteService.initSite({credential: this.site.getCredential(), latitude: coord.getLatitude(), longitude: coord.getLongitude()}).subscribe(
            (data: any) =>
                {
                    let site: Site = new Site(data["credential"], data["latitude"], data["longitude"]);
                    site.setId(data["id"]);
                    this.siteInitSubscription.next(site);
                },
                err => 
                {
                    this.errorSubscription.next(err);
                },
                () => console.log('Login operation complete'));
    }

    createSite(site: Site)
    {
        this.siteService.createSite(site).subscribe(
            (data: any) =>
            {
                this.siteCreateOrUpdateSubscription.next(1);
            },
            err => 
            {
                this.errorSubscription.next(err);
            },
            () => console.log('Login operation complete'));
    }

    updateSite(site: Site)
    {
        this.siteService.updateSite(site).subscribe(
            data => {
                this.siteCreateOrUpdateSubscription.next(2);
            },
            err => 
            {
                this.errorSubscription.next(err);
            },
            () => console.log('Login operation complete'));
    }

    getUserSites()
    {
        this.siteService.getUserSites().subscribe(
            (data: Array<any>) =>
            {
                let sites: Array<Site> = new Array();
                for (let i = 0; i < data.length; i++)
                {

                    let site: Site = new Site(data[i]["credential"], data[i]["latitude"], data[i]["longitude"]);
                    site.setId(data[i]["id"]);
                    site.setIsMySite(data[i]["isMySite"]);
                    let siteContactInfoObj = data[i]["SiteContactInfos"];
                    let siteSectionsObj = data[i]["SiteSections"];
                    let siteImagesObj = data[i]["SiteImages"];
                    site.setContactInfo(this.getContactInfoFromArray(siteContactInfoObj));
                    site.setSections(this.getSectionsFromArray(siteSectionsObj));
                    site.setImages(this.getImagesFromArray(siteImagesObj));
                    sites.push(site);
                }                
                this.userSitesSubscription.next(sites);
            }, err => {
                this.errorSubscription.next(err);
            }
        )
    }

    getNearestSites(coord: Coordinates, accuracy: number)
    {
        this.siteService.getNearestSites(coord, accuracy).subscribe(
            (data: Array<any>) =>
            {
                let sites: Array<Site> = new Array();
                for (let i = 0; i < data.length; i++)
                {

                    let site: Site = new Site(data[i]["credential"], data[i]["latitude"], data[i]["longitude"]);
                    site.setId(data[i]["id"]);
                    site.setIsMySite(data[i]["isMySite"]);
                    let siteContactInfoObj = data[i]["SiteContactInfos"];
                    let siteSectionsObj = data[i]["SiteSections"];
                    let siteImagesObj = data[i]["SiteImages"];
                    site.setContactInfo(this.getContactInfoFromArray(siteContactInfoObj));
                    site.setSections(this.getSectionsFromArray(siteSectionsObj));
                    site.setImages(this.getImagesFromArray(siteImagesObj));
                    sites.push(site);
                }                
                this.nearestSitesSubscription.next(sites);
            }, err => {
                this.errorSubscription.next(err);
            }
        )
    }

    getSiteById(id: number)
    {
        this.siteService.getSiteById(id).subscribe(
            (data: any) => 
            {
                let site: Site = new Site(data["credential"], data["latitude"], data["longitude"]);
                site.setId(data["id"]);
                site.setIsMySite(data["isMySite"]);
                let siteContactInfoObj = data["SiteContactInfos"];
                let siteSectionsObj = data["SiteSections"];
                let siteImagesObj = data["SiteImages"];
                site.setContactInfo(this.getContactInfoFromArray(siteContactInfoObj));
                site.setSections(this.getSectionsFromArray(siteSectionsObj));
                site.setImages(this.getImagesFromArray(siteImagesObj));
                this.siteByIdSubscription.next(site);
            }, err => {
                this.errorSubscription.next(err);
            }
        )
    }

    delete(id: number, type: string)
    {
        return new Promise((resolve: Function, reject: Function) => {
            switch(type)
            {
                case 'section':
                    this.siteService.deleteSection(id).subscribe(data=>resolve(data),error=>reject(error));
                    break;
                case 'image':
                    this.siteService.deleteImage(id).subscribe(data=>resolve(data),error=>reject(error));
                    break;
                case 'contact':
                    this.siteService.deleteContact(id).subscribe(data=>resolve(data),error=>reject(error));
                    break;
            }
        });
    }

    private getContactInfoFromArray(siteContactInfoObj: any): Array<ContactInfo>
    {
        let siteContacInfo: Array<ContactInfo> = new Array();
        for (let si = 0; si < siteContactInfoObj.length; si++)
        {
            let contact: ContactInfo = new ContactInfo(siteContactInfoObj[si]["id"], siteContactInfoObj[si]["language"]);
            contact.setData(siteContactInfoObj[si]["type"], siteContactInfoObj[si]["contact"]);
            siteContacInfo.push(contact);
        }
        return siteContacInfo;
    }

    private getSectionsFromArray(infoObj: any): Array<Section>
    {
        let siteSections: Array<Section> = new Array();
        for (let si = 0; si < infoObj.length; si++)
        {
            let section: Section = new Section(infoObj[si]["id"], infoObj[si]["title"], infoObj[si]["content"], infoObj[si]["language"]);
            siteSections.push(section);
        }
        return siteSections;
    }

    private getImagesFromArray(infoObj: any): Array<Image>
    {
        let images: Array<Image> = new Array();
        for (let si = 0; si < infoObj.length; si++)
        {
            let image: Image = new Image(infoObj[si]["alt"], infoObj[si]["file"], infoObj[si]["is_main"], infoObj[si]["id"]);
            images.push(image);
        }
        return images;
    }

    getSites()
    {
        
    }

    uploadImage()
    {

    }

    uploadSection()
    {
        
    }

    uploadContactInfo()
    {

    }
}