import { LoginStatus, User } from '../models/User';
import { UserService } from '../services/UserService';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Constants } from '../services/constants';
import { PublicApiService } from '../services/PublicApiCred';
import { ApiCredentials } from '../models/ApiCredentials';

export class PublicApiViewModel {
	private publicApiService: PublicApiService;

	public publicApiCredentialsCreate = new Subject<ApiCredentials>();
	public publicApiCredentialsGet = new Subject<ApiCredentials>();

	constructor(http: HttpClient) {
        this.publicApiService = new PublicApiService(http);
	}

    createNewPublicApiCredentials()
    {
        this.publicApiService.createApiCredentialsForUser({app: 'qbi-public-api'}).subscribe(
            (data: ApiCredentials) => {
				this.publicApiCredentialsCreate.next(data);
            });
    }

	getPublicApiCredentialsForUser() {
		this.publicApiService.getApiCredentialsForUser().subscribe(
			(data: ApiCredentials) => {
				this.publicApiCredentialsGet.next(data);
			});
	}
}