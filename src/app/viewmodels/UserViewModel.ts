import { LoginStatus, User } from '../models/User';
import { LoginService } from '../services/LoginService';
import { UserService } from '../services/UserService';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { LocalStorage } from '../services/LocalStorage';
import { Constants } from '../services/constants';
import { AuthProvider } from '../models/AuthProvider';

export class UserViewModel {
	private loginService: LoginService;
	private userService: UserService;
	private localStorage: LocalStorage;

	loginStatus: LoginStatus;
	userInfo: User;

	public loginStatusSubscription = new Subject<LoginStatus>();
	public currentUserSubscription = new Subject<User>();
	public userCreationSubscription = new Subject<User>();
	public authenticationProviderCreated = new Subject<AuthProvider>();
	public errorSubscription = new Subject<string>();
	constructor(http: HttpClient) {
		this.loginService = new LoginService(http);
		this.userService = new UserService(http);
		this.localStorage = new LocalStorage();
	}

	performLogin(body: { email: string, password: string }) {
		this.loginService.login(body).subscribe(
			(data: LoginStatus) => {
				this.loginStatus = data;
				this.localStorage.set(Constants.LocalStorageKey.QBI_LOGIN_TOKEN, data.loginToken());
				this.loginStatusSubscription.next(data);
			}, error => {
				this.errorSubscription.next(error['error']['message']);
			});
	}

	performLoginWithSocial(body: {providerKey: string, providerType: number}) {
		this.loginService.loginWithSocial(body).subscribe(
			(data: LoginStatus) => {
				this.loginStatus = data;
				this.localStorage.set(Constants.LocalStorageKey.QBI_LOGIN_TOKEN, data.loginToken());
				this.loginStatusSubscription.next(data);
			}, error => {
				this.errorSubscription.next(error['error']['message']);
			});
	}

	createUser(body: { email: string, password: string }) {
		let data: { email: string, password: string, type: number } = { email: body.email, password: body.password, type: 2 };
		this.userService.createUser(data).subscribe(
			(data: User) => {
				this.userCreationSubscription.next(data);
			}, error => {
				this.errorSubscription.next(error['error']['message']);
			});
	}

	currentUser() {
		this.userService.currentUser().subscribe(
			(data: User) => {
				this.userInfo = data;
				this.currentUserSubscription.next(data);
			});
	}

	createUserFromSocial(body: { email: string, password: string }) {
		let data: { email: string, password: string, type: number } = { email: body.email, password: body.password, type: 2 };
		return this.userService.createUser(data);
	}

	logout() {
		this.localStorage.set(Constants.LocalStorageKey.QBI_LOGIN_TOKEN, null);
	}

	createAuthenticationProvider(body: {email: string, providerKey: string; providerType: number; }) {
		this.userService.createAuthenticationProvider(body).subscribe(
			(data) => {
				if (data) {
					this.authenticationProviderCreated.next(data);
				}
			}, error => {
				this.errorSubscription.next(`${error['error']['message']}. Try to login instead`);
			});
	}

	getAvailableProvidersForUser():Observable<Array<AuthProvider>> {
		return this.userService.getUserAvailableAuthenticationProviders();
	}
}