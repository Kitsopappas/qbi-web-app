import { Constants } from '../services/constants';

export class Site
{
    public id?: number;
    public credential: string;
    public createdAt?: string;
    public updatedAt?: string;
    public coordinates: Coordinates;
    public deletedAt?: string;
    public images?: Array<Image>;
    public sections?: Array<Section>;
    public contactInfo?: Array<ContactInfo>;
    public isMySite?: boolean;

    constructor(credential: string, latitude: number, longitude: number)
    {
        this.credential = credential;
        this.coordinates = new Coordinates(latitude, longitude);
    }

    getId(): number
    {
        return this.id;
    }

    setId(id: number)
    {
        this.id = id;
    }

    setIsMySite(isMySite: boolean)
    {
        this.isMySite = isMySite;
    }
    getIsMySite(): boolean
    {
        return this.isMySite;
    }

    getCredential(): string
    {
        return this.credential;
    }

    getCreatedAt(): string
    {
        return this.createdAt;
    }

    getUpdatedAt(): string
    {
        return this.updatedAt;
    }

    getcoordinates(): Coordinates
    {
        return this.coordinates;
    }

    getDeletedAt(): string
    {
        return this.deletedAt;
    }

    setCoordinates(latitude: number, longitude: number)
    {
        this.coordinates = new Coordinates(latitude, longitude);
    }

    setCredential(credential: string)
    {
        this.credential = credential;
    }

    getImages(): Array<Image>
    {
        return this.images != null ? this.images : [];
    }

    deleteImages()
    {
        this.images.length = 0;
    }

    addImage(image: Image)
    {
        this.images.push(image);
    }

    setImages(images: Array<Image>)
    {
        this.images = images;
    }

    getSections(): Array<Section>
    {
        return this.sections != null ? this.sections : [];
    }

    deleteSections()
    {
        this.sections.length = 0;
    }

    addSection(section: Section)
    {
        this.sections.push(section);
    }

    setSections(sections: Array<Section>)
    {
        this.sections = sections;
    }

    removeSection(id: number)
    {
        let index = this.findSectionIndexById(id);
        if (index > 0 && index < this.sections.length)
        {
            this.sections.slice(index, 1);
        }
    }

    deleteImage(id: number)
    {
        let index = this.findImageIndexById(id);
        if (index > 0 && index < this.images.length)
        {
            this.images.slice(index, 1);
        }
    }

    getContactInfo(): Array<ContactInfo>
    {
        return this.contactInfo != null ? this.contactInfo : [];
    }

    addContactInfo(info: ContactInfo)
    {
        this.contactInfo.push(info);
    }

    setContactInfo(contactInfo: Array<ContactInfo>)
    {
        this.contactInfo = contactInfo;
    }

    deleteContactInfo(id: number)
    {
        let index = this.findContactInfoIndexById(id);
        if (index > 0 && index < this.contactInfo.length)
        {
            this.contactInfo.slice(index, 1);
        }
    }

    /* Helpers */
    private findImageIndexById(id: number): number
    {
        for(let i = 0; i < this.images.length; i++)
        {
            let image = this.images[i];
            if (image.getId() == id)
            {
                return i;
            }
        }
    }

    private findSectionIndexById(id: number): number
    {
        for(let i = 0; i < this.sections.length; i++)
        {
            let section = this.sections[i];
            if (section.getId() == id)
            {
                return i;
            }
        }
    }

    private findContactInfoIndexById(id: number): number
    {
        for(let i = 0; i < this.contactInfo.length; i++)
        {
            let section = this.contactInfo[i];
            if (section.getId() == id)
            {
                return i;
            }
        }
    }

}

export class Section
{
    public id?: number;
    public title: string;
    public content: string;
    public language: string;
    public level: number;

    constructor(id: number, title: string="", content: string="", language: string="en", level: number=0)
    {
        this.id = id;
        this.title = title;
        this.content = content;
        this.language = language;
        this.level = level;
    }

    getId(): number
    {
        return this.id;
    }

    getTitle(): string
    {
        return this.title;
    }

    getContent(): string
    {
        return this.content;
    }

    getLanguage(): string
    {
        return this.language;
    }

    getLevel(): number
    {
        return this.level;
    }

    setTitle(title: string)
    {
        this.title = title;
    }

    setContent(content: string)
    {
        this.content = content;
    }

    setLanguage(language: string)
    {
        this.language = language
    }

    setLevel(level: number)
    {   
        this.level = level;
    }
}

export class Image
{
    public id: number;
    public alt?: string;
    public file: string;
    public is_main: boolean;

    public fullPath: string;
    constructor(alt: string, file: string, is_main: boolean, id: number = -1)
    {
        this.id = id;
        this.alt = alt;
        this.file = file;
        this.is_main = is_main;
        this.fullPath = Constants.ApiUrl + Constants.ApiCall.QBI_SITE_IMAGES + file;
    }

    getId(): number
    {
        return this.id;
    }

    getAlt(): string
    {
        return this.alt;
    }

    getFile(): string
    {
        return this.file;
    }

    getIsMain(): boolean
    {
        return this.is_main;
    }

    setAlt(alt: string)
    {
        this.alt = alt;
    }

    setIsMain(is_main: boolean)
    {
        this.is_main = is_main;
    }
}

export class ContactInfo
{ 
    public id: number;
    public type: ContactInfoEnum;
    public contact: string;
    public language: string;

   constructor(id: number, language: string = "en")
   {
        this.id = id;
        this.language = language;
   }

   setData(type: ContactInfoEnum, contact: string)
   {
       this.type = type;
       this.contact = contact;
   }

   getId(): number
   {
       return this.id;
   }

   getType(): ContactInfoEnum
   {
       return this.type;
   }

   getContact(): string
   {
       return this.contact;
   }

   getLanguage(): string
   {
       return this.language;
   }

   setType(type: ContactInfoEnum)
   {
       this.type = type;
   }

   setContact(contact: string)
   {
       this.contact = contact;
   }

   setLanguage(language: string)
   {
       this.language = language;
   }
}

export class Coordinates
{
    public latitude: number;
    public longitude: number;

    constructor(latitude: number, longitude: number)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    getLatitude(): number
    {
        return this.latitude;
    }

    getLongitude(): number
    {
        return this.longitude;
    }

    setLatitude(latitude: number)
    {
        this.latitude = latitude;
    }

    setLongitude(longitude: number)
    {
        this.longitude = longitude;
    }
}
