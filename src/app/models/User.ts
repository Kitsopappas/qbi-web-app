export class User
{
    public email: string;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt?: string;
    public type: string;

    constructor(email: string, createdAt: string, updatedAt: string, deletedAt: string, type: string)
    {
        this.email = email;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.type = type;
    }

    getEmail(): string
    {
        return this.email;
    }

    getType(): string
    {
        return this.type;
    }
}

export class LoginStatus
{
    public success: boolean;
    public message: string;
    public token: string;
    public time: string;

    constructor(success: boolean, message: string, token: string, time: string)
    {
        this.success = success;
        this.message = message;
        this.token = token;
        this.time = time;
    }

    isLoggedIn(): boolean
    {
        return this.success;
    }

    loginToken(): string
    {
        return this.token;
    }

    loginTime(): string
    {
        return this.time;
    }

    loginMessage(): string
    {
        return this.message;
    }

}