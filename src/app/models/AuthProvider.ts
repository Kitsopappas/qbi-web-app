export class AuthProvider
{
    public providerKey: string;
    public providerType: number;

    constructor(providerKey: string, providerType: number)
    {
        this.providerKey = providerKey;
        this.providerType = providerType;
    }
}