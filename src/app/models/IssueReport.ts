export class IssueReport
{
    public type: string;
    public siteId: number;
    public reporterEmail: string;
    public title: string;
    public message: string;

    constructor(type: string, siteId: number, reporterEmail: string, title: string, message: string)
    {
        this.type = type;
        this.siteId = siteId;
        this.reporterEmail = reporterEmail;
        this.title = title;
        this.message = message;
    }
}