export class ApiCredentials
{
    public app: string;
    public publicKey: string;
    public privateKey: string;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;

    constructor(app: string, publicKey: string, privateKey: string, createdAt: string, updatedAt: string)
    {
        this.app = app;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}