import { Component, Input } from '@angular/core';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthProvider } from 'src/app/models/AuthProvider';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
})
export class LoginPage {
  private userViewModel: UserViewModel;

  public notificationType: string = "";
  public notificationMessage: string = "";
  public showNotification: boolean = false;

  public userWillSignUp: boolean = true;
  constructor(private http: HttpClient, private router: Router, private authService: SocialAuthService) {}

  ngOnInit() {
    this.bindWithViewModel();
  }

  bindWithViewModel(): void {
    this.userViewModel = new UserViewModel(this.http);
    this.userViewModel.currentUser();
    this.bindSubscribers();
  }

  bindSubscribers(): void {
    this.userViewModel.loginStatusSubscription.subscribe(data => {
      if (data["success"]) {
        this.displayNotification("Login Success!", "success");
        this.userViewModel.currentUser();
      }
      else {
        this.displayNotification("Incorrect credentials!", "error");
      }
    });

    this.userViewModel.currentUserSubscription.subscribe(data => {
      if (data) {
        this.router.navigate(['/']);
      }
    });

    this.userViewModel.userCreationSubscription.subscribe(data => {
      if (data) {
        this.userWillSignUp = false;
        this.displayNotification("New user created!", "success");
      }
    });

    this.userViewModel.errorSubscription.subscribe(data => {
      if (data) {
        this.displayNotification(data, "error");
      }
    });

    this.authService.authState.subscribe((user) => {
      if (this.userWillSignUp) {
        this.userViewModel.createUserFromSocial({ email: user.email, password: null }).subscribe((data) => {
          this.userViewModel.createAuthenticationProvider({ email: user.email, providerKey: user.id, providerType: user.provider == "FACEBOOK" ? 1 : 2 });
        }, error => {
          this.displayNotification(error['error']['message'], "error");
        });
      } else {
        this.userViewModel.performLoginWithSocial({ providerKey: user.id, providerType: user.provider == "FACEBOOK" ? 1 : 2 });
      }
    });

    this.userViewModel.authenticationProviderCreated.subscribe(
      (data: AuthProvider) => {
        if (data) {
          this.userViewModel.performLoginWithSocial({ providerKey: data.providerKey, providerType: data.providerType });
        }
      });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  // Actions
  performLogin(email: string, password: string): void {
    this.clearNotification();
    this.showNotification = false;
    if (this.validateEmail(email) && password) {
      this.userViewModel.performLogin({ email: email, password: password });
    } else {
      this.displayNotification("Incorrect credentials!", "error");
    }
  }

  performSignUp(email: string, password: string, rpassword: string): void {
    this.clearNotification();
    if (this.validateEmail(email) && password) {
      if (password !== rpassword) {
        this.displayNotification("Password missmatch!!", "error");
        return;
      }
      this.userViewModel.createUser({ email: email, password: password });
    } else {
      this.displayNotification("Something went wrong!", "error");
    }
  }

  toogleLoginSignUpPage(): void {
    this.userWillSignUp = !this.userWillSignUp;
  }

  private displayNotification(message: string, type: string) {
    this.notificationMessage = message;
    this.notificationType = type;
    this.showNotification = true;
  }

  private clearNotification(): void {
    this.notificationMessage = "";
    this.notificationType = "";
    this.showNotification = false;
  }

  private validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
