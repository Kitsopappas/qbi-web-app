import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { NotificationModule } from 'src/app/components/notification/qbi-notification.module';
import { Constants } from 'src/app/services/constants';
import { LoginPage } from './login';
import { LoginRouterModule } from './login-routing.module';
@NgModule({
    imports: [CommonModule, NotificationModule, FooterModule, FormsModule, SocialLoginModule],
    exports: [
        LoginPage,
        LoginRouterModule
    ],
    declarations: [LoginPage],
    providers: [{
        provide: 'SocialAuthServiceConfig',
        useValue: {
            autoLogin: false,
            providers: [
                {
                    id: GoogleLoginProvider.PROVIDER_ID,
                    provider: new GoogleLoginProvider(
                        Constants.SocialMedia.google.clientID
                    ),
                },
                {
                    id: FacebookLoginProvider.PROVIDER_ID,
                    provider: new FacebookLoginProvider(Constants.SocialMedia.facebook.clientID),
                }
            ],
        } as SocialAuthServiceConfig,
    }],
})
export class LoginPageModule {
}