import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Coordinates, Site } from 'src/app/models/Site';
import { SiteViewModel } from 'src/app/viewmodels/SiteViewModel';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';

@Component({
  selector: 'main',
  templateUrl: './main.html',
  styleUrls: ['./main.scss']
})
export class MainPage {
  private userViewModel: UserViewModel;
  private siteViewModel: SiteViewModel;

  nearestSites: Array<Site>;
  userIsLoggedIn: boolean = false;
  constructor(private http: HttpClient, private router: Router) {

  }

  ngOnInit() {
    this.bindWithViewModel();
  }

  bindWithViewModel(): void {
    this.userViewModel = new UserViewModel(this.http);
    this.siteViewModel = new SiteViewModel(this.http);
    this.userViewModel.currentUser();
    this.requestUserLocation();
    this.bindSubscribers();
  }

  bindSubscribers(): void {

    this.userViewModel.currentUserSubscription.subscribe(data => {
      if (data) {
        this.userIsLoggedIn = true;
      }
    });

    this.siteViewModel.nearestSitesSubscription.subscribe(data => {
      if (data) {
        this.nearestSites = data;
      }
    });
  }

  private requestUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const longitude = position.coords.longitude;
        const latitude = position.coords.latitude;
        this.siteViewModel.getNearestSites(new Coordinates(latitude, longitude), position.coords.accuracy);
      }, error => {
        const longitude = 39.6447055;
        const latitude = 20.034975;
        this.siteViewModel.getNearestSites(new Coordinates(latitude, longitude), 100);
      });
    } else {
      const longitude = 39.6447055;
      const latitude = 20.034975;
      this.siteViewModel.getNearestSites(new Coordinates(latitude, longitude), 100);
    }
  }
}
