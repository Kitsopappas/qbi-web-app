import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MainImageComponent } from 'src/app/components/main-image/main-image';
import { NavBarComponent } from 'src/app/components/navbar/navbar';
import { SiteCardComponent } from 'src/app/components/site-card/site-card';
import { MainPage } from './main';


describe('MainPageComponent', () => {
  let component: MainPage;
  let fixture: ComponentFixture<MainPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPage, MainImageComponent, SiteCardComponent,  NavBarComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
