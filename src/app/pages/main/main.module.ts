import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { MainImageModule } from 'src/app/components/main-image/main-image.module';
import { NavBarModule } from 'src/app/components/navbar/navbar.module';
import { SiteCardModule } from 'src/app/components/site-card/site-card.mocule';
import { MainPage } from './main';
import { MainPageRouterModule } from './main-routing.module';

@NgModule({
    imports: [CommonModule, FooterModule, MainImageModule, NavBarModule, SiteCardModule],
    exports: [MainPage, MainPageRouterModule],
    declarations: [MainPage],
    providers: [],
})
export class MainPageModule {
}
