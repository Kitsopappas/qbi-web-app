import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { ApiCredentials } from 'src/app/models/ApiCredentials';
import { AuthProvider } from 'src/app/models/AuthProvider';
import { Site } from 'src/app/models/Site';
import { User } from 'src/app/models/User';
import { PublicApiViewModel } from 'src/app/viewmodels/PublicApiViewModel';
import { SiteViewModel } from 'src/app/viewmodels/SiteViewModel';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';

@Component({
  selector: 'admin-panel',
  templateUrl: './AdminPanel.html',
  styleUrls: ['./AdminPanel.scss']
})
export class AdminPanel {

  private userViewModel: UserViewModel;
  private siteViewModel: SiteViewModel;
  private publicApiViewModel: PublicApiViewModel;
  public userSites: Array<Site>;

  userIsLoggedIn: boolean = false;

  userInfo: User;
  publicApiCredentials: ApiCredentials;
  hasPublicApi: boolean = false;
  userHasFacebookLogin: boolean = false;
  userHasGoogleLogin: boolean = false;
  constructor(private http: HttpClient, private router: Router, private authService: SocialAuthService) {
    this.publicApiCredentials = new ApiCredentials("", "", "", "", "");
  }

  ngOnInit() {
    this.bindWithViewModel();
  }

  bindWithViewModel(): void {
    this.userViewModel = new UserViewModel(this.http);
    this.siteViewModel = new SiteViewModel(this.http);
    this.publicApiViewModel = new PublicApiViewModel(this.http);
    this.userViewModel.currentUser();
    this.bindSubscribers();
  }

  bindSubscribers(): void {
    this.userViewModel.currentUserSubscription.subscribe(data => {
      if (data) {
        this.userInfo = data;
        this.userIsLoggedIn = true;
        this.siteViewModel.getUserSites();
        this.publicApiViewModel.getPublicApiCredentialsForUser();
        this.getUserAvailableProviders();
      }
    });

    this.siteViewModel.userSitesSubscription.subscribe(data => {
      if (data) {
        this.userSites = data;
      }
    });

    this.publicApiViewModel.publicApiCredentialsGet.subscribe(data => {
      if (data) {
        this.publicApiCredentials = data;
        this.hasPublicApi = true;
      }
    });

    this.publicApiViewModel.publicApiCredentialsCreate.subscribe(data => {
      if (data) {
        this.publicApiCredentials = data;
        this.hasPublicApi = true;
      }
    });

    this.authService.authState.subscribe((user) => {
      if (user.email === this.userInfo.email) {
        this.userViewModel.createAuthenticationProvider({ email: user.email, providerKey: user.id, providerType: user.provider == "FACEBOOK" ? 1 : 2 });
      }
    });
  }

  getUserAvailableProviders(): void {
    this.userViewModel.getAvailableProvidersForUser().subscribe((providers: Array<AuthProvider>) => {
      console.log(providers);
      for(let i = 0; i < providers.length; i++)
      {
        let provider: AuthProvider = providers[i];
        if (provider.providerType == 1)
        {
          this.userHasFacebookLogin = true;
        }

        if (provider.providerType == 2)
        {
          this.userHasGoogleLogin = true;
        }
      }
    });
  }

  didTouchAddSite() {
    this.router.navigate(["/add-site"]);
  }

  didTouchGetPubicApiCredentials() {
    if (!this.publicApiViewModel) {
      return;
    }

    this.publicApiViewModel.createNewPublicApiCredentials();
  }

  addAuthenticationWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  addAuthenticationWithFacebook(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  didTouchLogout() {
    this.userViewModel.logout();
    this.userIsLoggedIn = false;
    this.router.navigate(['/']);
  }
}
