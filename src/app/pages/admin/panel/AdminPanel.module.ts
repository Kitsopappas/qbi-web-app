import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { AppApiModule } from 'src/app/components/app-api/app-api.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { MainImageModule } from 'src/app/components/main-image/main-image.module';
import { NavBarModule } from 'src/app/components/navbar/navbar.module';
import { SiteCardModule } from 'src/app/components/site-card/site-card.mocule';
import { Constants } from 'src/app/services/constants';
import { AdminPanel } from './AdminPanel';
import { AdminPanelRouterModule } from './AdminPanel-routing.module';

@NgModule({
    imports: [CommonModule, FooterModule, MainImageModule, NavBarModule, SiteCardModule, AppApiModule, FormsModule, SocialLoginModule],
    exports: [AdminPanel, AdminPanelRouterModule],
    declarations: [AdminPanel],
    providers: [{
        provide: 'SocialAuthServiceConfig',
        useValue: {
            autoLogin: false,
            providers: [
                {
                    id: GoogleLoginProvider.PROVIDER_ID,
                    provider: new GoogleLoginProvider(
                        Constants.SocialMedia.google.clientID
                    ),
                },
                {
                    id: FacebookLoginProvider.PROVIDER_ID,
                    provider: new FacebookLoginProvider(Constants.SocialMedia.facebook.clientID),
                }
            ],
        } as SocialAuthServiceConfig,
    }],
})
export class AdminPanelModule {
}