import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPanel } from './AdminPanel';

const routes: Routes = [
    {
        path: '',
        component: AdminPanel
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminPanelRouterModule { }