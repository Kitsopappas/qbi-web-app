import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSite } from './AddSite';

const routes: Routes = [
    {
        path: '',
        component: AddSite
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AddSiteRouterModule { }