import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { FileSaverModule } from 'ngx-filesaver';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { MainImageModule } from 'src/app/components/main-image/main-image.module';
import { MapModule } from 'src/app/components/map/map.module';
import { NavBarModule } from 'src/app/components/navbar/navbar.module';
import { NotificationModule } from 'src/app/components/notification/qbi-notification.module';
import { AddSite } from './AddSite';
import { AddSiteRouterModule } from './AddSite-routing.module';

@NgModule({
    imports: [
        CommonModule, 
        FooterModule, 
        MainImageModule, 
        NavBarModule, 
        MapModule, 
        FormsModule, 
        FileUploadModule, 
        FileSaverModule,
        NotificationModule
    ],
    exports: [AddSite, AddSiteRouterModule],
    declarations: [AddSite],
    providers: [],
})
export class AddSiteModule {
}