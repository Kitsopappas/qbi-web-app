import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MapComponent } from 'src/app/components/map/map';
import { User } from 'src/app/models/User';
import { ContactInfo, Coordinates, Image, Section, Site } from 'src/app/models/Site';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';
import { SiteViewModel } from 'src/app/viewmodels/SiteViewModel';
import { FileItem } from 'ng2-file-upload';

@Component({
	selector: 'add-site',
	templateUrl: './AddSite.html',
	styleUrls: ['./AddSite.scss']
})
export class AddSite {

	private userViewModel: UserViewModel;
	public siteViewModel: SiteViewModel;

	public coordinates: Coordinates;
	public userCoordinates: Coordinates;
	public sections: Array<Section>;
	public contactInfo: Array<ContactInfo>;
	public images: Array<Image>;
	public siteModel: Site;
	@ViewChild(MapComponent) mapViewChild;

	userIsLoggedIn: boolean = false;

	userInfo: User;

	public notificationType: string = "";
	public notificationMessage: string = "";
	public showNotification: boolean = false;
	constructor(private http: HttpClient, private router: Router) {
		this.siteModel = new Site("", 0, 0);
		this.sections = new Array();
		this.contactInfo = new Array();
		this.images = new Array();
	}

	ngOnInit() {
		this.bindWithViewModel();
	}

	bindWithViewModel(): void {
		this.userViewModel = new UserViewModel(this.http);
		this.siteViewModel = new SiteViewModel(this.http);
		this.requestUserLocation();
		this.userViewModel.currentUser();
		this.bindSubscribers();
		this.bindFileUploader();
	}

	bindSubscribers(): void {

		this.userViewModel.currentUserSubscription.subscribe(data => {
			if (data) {
				this.userInfo = data;
				this.userIsLoggedIn = true;
			}
		});

		this.siteViewModel.siteInitSubscription.subscribe(data => {
			if (data) {
				this.siteModel = data;
			}
		});

		this.siteViewModel.siteCreateOrUpdateSubscription.subscribe(data => {
			if (data == 1) {
				this.notificationType = 'success';
				this.notificationMessage = 'Site created successfully!';
				this.showNotification = true;
			}
		});
	}

	bindFileUploader() {
		if (!this.siteViewModel) {
			return;
		}

		this.siteViewModel.fileUploaderService.onFileAdded.subscribe((file: FileItem) => {
		});

		this.siteViewModel.fileUploaderService.onFileUploaded.subscribe(data => {
			if (data.status == 200) {
				const res = JSON.parse(data.response);
				this.images.push(new Image(res.fileName, res.fileName, false));
			}
		});
	}

	didTouchAddSection() {
		this.sections.push(new Section(-1));
	}

	didTouchAddContactInfo() {
		this.contactInfo.push(new ContactInfo(-1));
	}

	didTouchInitSite() {
		if (this.coordinates.getLatitude && this.coordinates.getLongitude && this.siteModel.getCredential()) {
			this.siteModel.setCoordinates(this.coordinates.getLatitude(), this.coordinates.getLongitude());
			this.siteViewModel.initSite(this.siteModel);
		}
	}

	didTouchUpdateSite() {
		this.siteModel.setSections(this.sections);
		this.siteModel.setContactInfo(this.contactInfo);
		this.siteModel.setImages(this.images);
		this.siteViewModel.createSite(this.siteModel);
	}

	receivedNewCoordinates($event) {
		this.coordinates = $event;
	}

	private requestUserLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				const longitude = position.coords.longitude;
				const latitude = position.coords.latitude;
				this.userCoordinates = new Coordinates(latitude, longitude);
			}, (error) => {
			});
		} else {
			console.error("No support for geolocation")
		}
	}
}
