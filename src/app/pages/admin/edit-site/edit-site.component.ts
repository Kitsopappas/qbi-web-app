import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileItem } from 'ng2-file-upload';
import { ContactInfo, Image, Section, Coordinates, Site } from 'src/app/models/Site';
import { User } from 'src/app/models/User';
import { SiteViewModel } from 'src/app/viewmodels/SiteViewModel';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';

@Component({
	selector: 'app-edit-site',
	templateUrl: './edit-site.component.html',
	styleUrls: ['./edit-site.component.scss']
})
export class EditSiteComponent implements OnInit {
	private userViewModel: UserViewModel;
	public siteViewModel: SiteViewModel;

	public coordinates: Coordinates;
	public sections: Array<Section>;
	public contactInfo: Array<ContactInfo>;
	public images: Array<Image>;

	latLng: { lat: number, lon: number };
	userIsLoggedIn: boolean = false;
	userInfo: User;
	public siteModel: Site;

	public notificationType: string = "";
	public notificationMessage: string = "";
	public showNotification: boolean = false;
	constructor(private http: HttpClient, private router: Router, private aRoute: ActivatedRoute) { }

	ngOnInit(): void {
		this.bindWithViewModel();
	}

	bindWithViewModel(): void {
		this.userViewModel = new UserViewModel(this.http);
		this.siteViewModel = new SiteViewModel(this.http);
		this.userViewModel.currentUser();
		this.bindSubscribers();
		this.bindFileUploader();
	}

	private bindSubscribers(): void {
		this.userViewModel.currentUserSubscription.subscribe(data => {
			if (data) {
				this.userInfo = data;
				this.userIsLoggedIn = true;
				let id: number = +this.aRoute.snapshot.paramMap.get("id");
				if (id) {
					this.siteViewModel.getSiteById(id);
				}
			}
		});

		this.siteViewModel.siteByIdSubscription.subscribe(data => {
			if (data) {
				this.siteModel = data;
				this.sections = this.siteModel.getSections();
				this.contactInfo = this.siteModel.getContactInfo();
				this.coordinates = this.siteModel.getcoordinates();
				this.images = this.siteModel.getImages();
				this.latLng = { lat: this.coordinates.getLatitude(), lon: this.coordinates.getLongitude() };
			}
		});

		this.siteViewModel.siteCreateOrUpdateSubscription.subscribe(data => {
			if (data == 2) {
				this.notificationType = 'success';
				this.notificationMessage = 'Site updated successfully!';
				this.showNotification = true;
				let id: number = +this.aRoute.snapshot.paramMap.get("id");
				if (id) {
					this.siteViewModel.getSiteById(id);
				}
			}
		});
	}

	bindFileUploader() {
		if (!this.siteViewModel) {
			return;
		}

		this.siteViewModel.fileUploaderService.onFileAdded.subscribe((file: FileItem) => {
		});

		this.siteViewModel.fileUploaderService.onFileUploaded.subscribe(data => {
			if (data.status == 200) {
				const res = JSON.parse(data.response);
				this.images.push(new Image(res.fileName, res.fileName, false));
			}
		});
	}

	didTouchUpdateSite() {
		this.siteModel.setSections(this.sections);
		this.siteModel.setContactInfo(this.contactInfo);
		this.siteModel.setImages(this.images);
		this.siteViewModel.updateSite(this.siteModel);
	}

	didTouchAddSection() {
		this.sections.push(new Section(-1));
	}

	didTouchAddContactInfo() {
		this.contactInfo.push(new ContactInfo(-1));
	}

	didTouchDeleteSection(id: number) {
		this.siteViewModel.delete(id, 'section').then((r) => {
			const index = this.findSectionIndexById(id);
			if (index >= 0) this.sections.splice(index, 1);
		}).catch((e) => {
			/* TODO */
		});
	}

	didTouchDeleteImage(id: number) {
		this.siteViewModel.delete(id, 'image').then((r) => {
			const index = this.findImageIndexById(id);
			if (index >= 0) this.images.splice(index, 1);
		}).catch((e) => {
			/* TODO */
		});
	}

	didTouchDeleteContact(id: number) {
		this.siteViewModel.delete(id, 'contact').then((r) => {
			const index = this.findContactIndexById(id);
			if (index >= 0) this.contactInfo.splice(index, 1);
		}).catch((e) => {
			/* TODO */
		});
	}

	private findSectionIndexById(id: number): number {
		for (let i = 0; i < this.sections.length; i++) {
			if (this.sections[i].getId() === id) {
				return i;
			}
		}
		return -1;
	}

	private findImageIndexById(id: number): number {
		for (let i = 0; i < this.images.length; i++) {
			if (this.images[i].getId() === id) {
				return i;
			}
		}
		return -1;
	}

	private findContactIndexById(id: number): number {
		for (let i = 0; i < this.contactInfo.length; i++) {
			if (this.contactInfo[i].getId() === id) {
				return i;
			}
		}
		return -1;
	}

}