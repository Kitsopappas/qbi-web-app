import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditSiteComponent } from './edit-site.component';

const routes: Routes = [
    {
        path: '',
        component: EditSiteComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditSiteComponentRouterModule { }