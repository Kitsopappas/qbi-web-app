import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { FileSaverModule } from 'ngx-filesaver';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { MainImageModule } from 'src/app/components/main-image/main-image.module';
import { MapModule } from 'src/app/components/map/map.module';
import { NavBarModule } from 'src/app/components/navbar/navbar.module';
import { NotificationModule } from 'src/app/components/notification/qbi-notification.module';
import { EditSiteComponentRouterModule } from './edit-site-routing.module';
import { EditSiteComponent } from './edit-site.component';

@NgModule({
    imports: [
        CommonModule, 
        FooterModule, 
        MainImageModule, 
        NavBarModule, 
        MapModule, 
        FormsModule, 
        FileUploadModule, 
        FileSaverModule,
        NotificationModule
    ],
    exports: [EditSiteComponent, EditSiteComponentRouterModule],
    declarations: [EditSiteComponent],
    providers: [],
})
export class EditSiteModule {
}