import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { MainImageModule } from 'src/app/components/main-image/main-image.module';
import { MapModule } from 'src/app/components/map/map.module';
import { NavBarModule } from 'src/app/components/navbar/navbar.module';
import { NotificationModule } from 'src/app/components/notification/qbi-notification.module';
import { SiteViewPage } from './SiteView';
import { SiteViewPageRouterModule } from './SiteView-routing.module';

@NgModule({
    imports: [CommonModule, MainImageModule, NavBarModule, FooterModule, MapModule, QRCodeModule, NotificationModule, FormsModule],
    exports: [SiteViewPage, SiteViewPageRouterModule],
    declarations: [SiteViewPage],
    providers: [],
})
export class SiteViewPageModule {
}