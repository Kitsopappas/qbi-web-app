import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteViewPage } from './SiteView';

const routes: Routes = [
    {
        path: '',
        component: SiteViewPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SiteViewPageRouterModule { }