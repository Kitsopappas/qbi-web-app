import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileSaverService } from "ngx-filesaver";
import { IssueReport } from 'src/app/models/IssueReport';
import { Section, Site } from 'src/app/models/Site';
import { IssueReportViewModel } from 'src/app/viewmodels/IssueReportViewModel';
import { SiteViewModel } from 'src/app/viewmodels/SiteViewModel';
import { UserViewModel } from 'src/app/viewmodels/UserViewModel';

@Component({
	selector: 'siteview',
	templateUrl: './SiteView.html',
	styleUrls: ['./SiteView.scss']
})
export class SiteViewPage {
	private userViewModel: UserViewModel;
	private siteViewModel: SiteViewModel;
	private issueReportViewModel: IssueReportViewModel;
	public site: Site;
	userIsLoggedIn: boolean = false;
	isMySite: boolean = false;
	urlForQr: string;
	availableLng: Array<string>;
	selectedLng: string;

	issueReportModel: IssueReport;
	public notificationType: string = "";
	public notificationMessage: string = "";
	public showNotification: boolean = false;
	constructor(private http: HttpClient, private router: Router, private aRoute: ActivatedRoute, private fileSaverService: FileSaverService) {
	}

	ngOnInit() {
		this.selectedLng = "gr";
		this.bindWithViewModel();
	}

	bindWithViewModel(): void {
		this.userViewModel = new UserViewModel(this.http);
		this.siteViewModel = new SiteViewModel(this.http);
		this.issueReportViewModel = new IssueReportViewModel(this.http);
		this.userViewModel.currentUser();

		let id: number = +this.aRoute.snapshot.paramMap.get("id");
		if (id) {
			this.issueReportModel = new IssueReport("", id, "", "", "");
			this.siteViewModel.getSiteById(id);
		}

		const parsedUrl = new URL(window.location.href);
		this.urlForQr = parsedUrl.href;
		this.bindSubscribers();
	}

	bindSubscribers(): void {

		this.userViewModel.currentUserSubscription.subscribe(data => {
			if (data) {
				this.userIsLoggedIn = true;
			}
		});

		this.siteViewModel.siteByIdSubscription.subscribe(data => {
			if (data) {
				this.site = data;
				this.isMySite = this.site.getIsMySite();
				this.findAvailableLanguagesForSite();
			}
		});

		this.issueReportViewModel.issueReported.subscribe(data => {
			if (data) {
				this.notificationType = "success";
				this.notificationMessage = "Thank you for your report! We will investigate the issue.";
				this.showNotification = true;
			}
		});
	}

	private findAvailableLanguagesForSite() {
		let sections: Array<Section> = this.site.getSections();
		let availableLng = new Array<string>();
		for (let i = 0; i < sections.length; i++) {
			const section: Section = sections[i];
			availableLng.push(section.getLanguage());
		}
		this.availableLng = [...new Set(availableLng)];
	}

	didTouchGetQrCode() {
		let obj: any = document.getElementById('qrcodesvg');
		let svgBody: any = obj.querySelector("svg").innerHTML;
		let blob = new Blob([svgBody]);
		this.fileSaverService.save(blob, 'qbi_my_qrcode.svg');
	}

	didTouchEditSite(): void {
		this.router.navigate([`/edit/${this.site.getId()}`]);
	}

	didTouchReportAnIssue(): void {
		if (!this.validateEmail(this.issueReportModel.reporterEmail)) {
			this.notificationType = "error";
			this.notificationMessage = "Please provide us with an email";
			this.showNotification = true;
			return;
		}
		if (this.issueReportModel.type == "") {
			this.notificationType = "error";
			this.notificationMessage = "Please provide us with a report type";
			this.showNotification = true;
			return;
		}
		this.issueReportViewModel.createNewPublicApiCredentials(this.issueReportModel);
	}

	private validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
}
